# PDDL 

Here you can find tools and materials required to finish the PDDL-related assignments:

- sources of FastForward planner (for Windows and Unix). 

- precompiled binaries for several platforms:
  - linux-64bit
  - linux-32bit
  - macOS
  - windows-32bit (should also work on 64bit)
- assignments and examples put in `lab1` and `lab2` directories

## How To Build Your Own FF

### Requirements

- Windows (install via Cygwin):
  - mingw64
  - bison
  - flex
  - automake
- Unix:
  - gcc (not clang, on macOS you can use gcc provided by anaconda)
  - bison
  - flex
  - automake
  
### Building

Change directory to the appriopriate src directory and run ```make```.
The `ff` binary should magically appear ;)
